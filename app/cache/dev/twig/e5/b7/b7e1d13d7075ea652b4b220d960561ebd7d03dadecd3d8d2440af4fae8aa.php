<?php

/* ::base.html.twig */
class __TwigTemplate_e5b7b7e1d13d7075ea652b4b220d960561ebd7d03dadecd3d8d2440af4fae8aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'navigation' => array($this, 'block_navigation'),
            'blog_title' => array($this, 'block_blog_title'),
            'blog_tagline' => array($this, 'block_blog_tagline'),
            'body' => array($this, 'block_body'),
            'sidebar' => array($this, 'block_sidebar'),
            'footer' => array($this, 'block_footer'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=\"Content-Type\" content=\"text/html\"; charset=utf-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo " - sym2blog</title>
        <!--[if lt IE 9]>
            <script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
        <![endif]-->
        ";
        // line 9
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 18
        echo "        <link rel=\"shortcut icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>

        <section id=\"wrapper\">
            <header id=\"header\">
                <div class=\"top\">
                    ";
        // line 25
        $this->displayBlock('navigation', $context, $blocks);
        // line 34
        echo "                </div>

                <hgroup>
                    <h2>";
        // line 37
        $this->displayBlock('blog_title', $context, $blocks);
        echo "</h2>
                    <h3>";
        // line 38
        $this->displayBlock('blog_tagline', $context, $blocks);
        echo "</h3>
                </hgroup>
            </header>

            <section class=\"main-col\">
            ";
        // line 43
        $this->displayBlock('body', $context, $blocks);
        // line 44
        echo "        </section>
        <aside class=\"sidebar\">
        ";
        // line 46
        $this->displayBlock('sidebar', $context, $blocks);
        // line 47
        echo "    </aside>

    <div id=\"footer\">
        ";
        // line 50
        $this->displayBlock('footer', $context, $blocks);
        // line 53
        echo "    </div>
</section>

";
        // line 56
        $this->displayBlock('javascripts', $context, $blocks);
        // line 63
        echo "</body>
</html>";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo "sym2blog";
    }

    // line 9
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 10
        echo "            <link href='http://fonts.googleapis.com/css?family=Irish+Grover' rel='stylesheet' type='text/css'>
            <link href='http://fonts.googleapis.com/css?family=La+Belle+Aurore' rel='stylesheet' type='text/css'>
            ";
        // line 12
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "51c56cc_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_51c56cc_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/51c56cc_part_1_screen_1.css");
            // line 15
            echo "                <link href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" rel=\"stylesheet\" media=\"screen\" />
            ";
        } else {
            // asset "51c56cc"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_51c56cc") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/51c56cc.css");
            echo "                <link href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" rel=\"stylesheet\" media=\"screen\" />
            ";
        }
        unset($context["asset_url"]);
        // line 17
        echo "        ";
    }

    // line 25
    public function block_navigation($context, array $blocks = array())
    {
        // line 26
        echo "                        <nav>
                            <ul class=\"navigation\">
                                <li><a href=\"";
        // line 28
        echo $this->env->getExtension('routing')->getPath("blogger_blog_homepage");
        echo "\">Home</a></li>
                                <li><a href=\"";
        // line 29
        echo $this->env->getExtension('routing')->getPath("blogger_blog_about");
        echo "\">About</a></li>
                                <li><a href=\"";
        // line 30
        echo $this->env->getExtension('routing')->getPath("blogger_blog_contact");
        echo "\">Contact</a></li>
                            </ul>
                        </nav>
                    ";
    }

    // line 37
    public function block_blog_title($context, array $blocks = array())
    {
        echo "<a href=\"";
        echo $this->env->getExtension('routing')->getPath("blogger_blog_homepage");
        echo "\">sym2blog</a>";
    }

    // line 38
    public function block_blog_tagline($context, array $blocks = array())
    {
        echo "<a href=\"";
        echo $this->env->getExtension('routing')->getPath("blogger_blog_homepage");
        echo "\">just another blog in Symfony2</a>";
    }

    // line 43
    public function block_body($context, array $blocks = array())
    {
    }

    // line 46
    public function block_sidebar($context, array $blocks = array())
    {
    }

    // line 50
    public function block_footer($context, array $blocks = array())
    {
        // line 51
        echo "            Symfony2 blog - created by <a href=\"https://bitbucket.org/m2015/sym2blog\">Emanuele Amico</a>
        ";
    }

    // line 56
    public function block_javascripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  192 => 56,  187 => 51,  184 => 50,  179 => 46,  174 => 43,  166 => 38,  158 => 37,  150 => 30,  146 => 29,  142 => 28,  138 => 26,  135 => 25,  131 => 17,  117 => 15,  113 => 12,  109 => 10,  106 => 9,  100 => 5,  95 => 63,  93 => 56,  88 => 53,  86 => 50,  81 => 47,  79 => 46,  75 => 44,  73 => 43,  65 => 38,  61 => 37,  56 => 34,  54 => 25,  43 => 18,  41 => 9,  34 => 5,  28 => 1,);
    }
}
